    ({expect} = require 'chai').should()

    describe 'The module', ->
      it 'should load', -> require '..'

      it 'should contain a couchapp', ->
        {couchapp} = require '..'
        couchapp().should.have.property 'views'
        couchapp().views.should.have.property 'by_host'
        couchapp().views.by_host.should.have.property 'map'
        expect(couchapp().views.by_host.map).to.be.a 'string'

      it 'should contain a couchapp', ->
        {couchapp} = require '..'
        o = default_registrant_expiry: 42
        couchapp(o).should.have.property 'views'
        couchapp(o).views.should.have.property 'by_host'
        couchapp(o).views.by_host.should.have.property 'map'
        expect(couchapp(o).views.by_host.map).to.be.a 'string'
