    app_name = 'ccnq4-registrant'

    {version} = require './package.json'
    {major,minor} = require 'semver'

    app_version = "#{major version}.#{minor version}"

    app = "#{app_name}-#{app_version}"

    {p_fun} = require 'coffeescript-helpers'

    couchapp = (cfg) ->

      options =
        default_registrant_expiry: cfg?.default_registrant_expiry ? 86413

      extra = """
        var default_registrant_expiry = #{options.default_registrant_expiry};
      """

      by_host = require('./by_host') options

      _id: "_design/#{app}"
      version: version
      language: 'javascript'
      views:
        by_host:
          map: p_fun extra, by_host

    module.exports = {app,app_version,couchapp}
